rootProject.name = "luckperms-rest-api"

pluginManagement {
    repositories {
        mavenLocal()
        maven("https://nexus.jeikobu.net/repository/maven-releases/")
        gradlePluginPortal()
    }
}