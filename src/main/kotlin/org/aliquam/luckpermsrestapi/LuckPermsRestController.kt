package org.aliquam.luckpermsrestapi

import com.fasterxml.jackson.annotation.JsonProperty
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.response.*
import io.ktor.routing.*
import net.luckperms.api.LuckPerms
import net.luckperms.api.context.ImmutableContextSet
import net.luckperms.api.query.QueryOptions
import org.bukkit.Bukkit
import org.slf4j.event.Level
import java.util.*

const val WORLD_KEY = "world"
const val SERVER_KEY = "server"
const val UUID_KEY = "uuid"
const val PERMISSION_KEY = "permissionName"

object LuckPermsRestController {
    private val api: LuckPerms = Bukkit.getServicesManager().getRegistration(LuckPerms::class.java)?.provider
            ?: throw InstantiationException("Bukkit has NOT been initialized!")

    fun Application.module() {
        install(ContentNegotiation) {
            register(ContentType.Any, JacksonConverter())
            register(ContentType.Application.Json, JacksonConverter())
        }

        install(CallLogging) {
            level = Level.INFO
        }

        routing {
            get("/api/players/player/{$UUID_KEY}/permissions/{$PERMISSION_KEY}") {
                val uuid = UUID.fromString(call.parameters[UUID_KEY])
                val permissionName = call.parameters[PERMISSION_KEY]!!

                val worldName: String? = call.request.queryParameters[WORLD_KEY]
                val serverName: String? = call.request.queryParameters[SERVER_KEY]

                val queryOptions = QueryOptions.contextual(getContextSet(worldName, serverName))
                val permissionData = getLuckPermsUser(uuid).cachedData.getPermissionData(queryOptions)

                if (permissionData.checkPermission(permissionName).asBoolean()) {
                    return@get call.respond(HttpStatusCode.OK)
                } else {
                    return@get call.respond(HttpStatusCode.Forbidden)
                }
            }

            get("/api/players/player/{$UUID_KEY}/groups") {
                val uuid = UUID.fromString(call.parameters[UUID_KEY])
                val worldName: String? = call.request.queryParameters[WORLD_KEY]
                val serverName: String? = call.request.queryParameters[SERVER_KEY]

                val user = getLuckPermsUser(uuid)

                val queryOptions = QueryOptions.contextual(getContextSet(worldName, serverName))
                val groups = user.getInheritedGroups(queryOptions).map { it.name }

                return@get call.respond(Groups(groups))
            }

            get("/api/players/player/{$UUID_KEY}/maingroup")  {
                return@get call.respond(PrimaryGroup(
                    getLuckPermsUser(
                        UUID.fromString(call.parameters[UUID_KEY])
                    ).primaryGroup)
                )
            }
        }
    }

    fun getLuckPermsUser(uuid: UUID) = api.userManager.loadUser(uuid).join()

    fun getContextSet(world: String? = null, server: String? = null): ImmutableContextSet {
        val builder = ImmutableContextSet.builder()
        if (world != null) builder.add(WORLD_KEY, world)
        if (server != null) builder.add(SERVER_KEY, server)

        return builder.build()
    }
}

data class Groups(@JsonProperty("groups") var groups: List<String>)
data class PrimaryGroup(@JsonProperty("groupName") var primaryGroup: String)