package org.aliquam.luckpermsrestapi

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.aliquam.luckpermsrestapi.LuckPermsRestController.module
import org.bukkit.plugin.java.JavaPlugin
import org.slf4j.LoggerFactory

const val LOGGER_NAME = "LuckPerms-REST-API"

class LuckPermsRestPlugin : JavaPlugin() {
    override fun onEnable() {
        embeddedServer(Netty, environment = applicationEngineEnvironment {
            module {
                this.module()
            }

            connector {
                port = 2200
            }

            log = LoggerFactory.getLogger(LOGGER_NAME)
        }).start()

        server.clearRecipes()
        server.worlds.forEach { server.unloadWorld(it, false) }
    }
}