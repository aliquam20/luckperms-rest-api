#!/usr/bin/python3
from alq_server_jumpstarter import alq_server_jumpstarter as starter
import os

server_name = "LuckPerms-REST-API"
server_port = 25565
server_dir = "."
server_jar = "paper.jar"
server_memory_start = "256M"
server_memory_limit = "384M"

def configure(server_name, server_port):
	plugins = ["LuckPerms"]
	for plugin in plugins:
		plugin_path = os.path.join("plugins", plugin, "config.yml")

		starter.replace_in(plugin_path, [
			("%ALQ_DB_HOST%", starter.get_env("ALQ_MC_DB_HOST")),
			("%ALQ_DB_PORT%", starter.get_env("ALQ_MC_DB_PORT")),
			("%ALQ_DB_USER%", starter.get_env("ALQ_MC_DB_USER")),
			("%ALQ_DB_PASS%", starter.get_env("ALQ_MC_DB_PASS")),
			("%ALQ_DB_SCHEMA%", starter.get_env("ALQ_MC_DB_SCHEMA"))
		])


configure(server_name, server_port)
starter.start_server(server_name, server_dir, server_jar, server_memory_start, server_memory_limit)