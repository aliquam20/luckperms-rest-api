import com.bmuschko.gradle.docker.tasks.image.Dockerfile
import com.bmuschko.gradle.docker.tasks.image.DockerBuildImage
import com.bmuschko.gradle.docker.tasks.image.DockerPushImage
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.aliquam.AlqUtils

plugins {
    id("org.aliquam.alq-gradle-parent") version "0.4.14"
    id("com.bmuschko.docker-remote-api") version "6.7.0"
    id("org.sonarqube") version "3.0"
    kotlin("jvm") version "1.4.30"
}

repositories {
    mavenCentral()
    maven("https://jitpack.io")
    maven("https://papermc.io/repo/repository/maven-public/")
}

val alq = AlqUtils(project).withStandardProjectSetup()
val unzip = configurations.create("archive")

val baseVersion = "1.0.0"

val buildWorkDir = "build/work"
val buildDockerDir = "build/docker/work"
val serverConfigDir = "src/mcserver/config"

val imageName = "aliquam/luckperms-rest-api"

val registryName = System.getenv("DOCKER_REGISTRY_HOST") ?: ""
val branchName: String? = System.getenv("BRANCH_NAME")
val buildNumber: String? = System.getenv("BUILD_NUMBER")

group = "org.aliquam"
version = alq.getSemVersion(if (buildNumber != null) "$baseVersion-$buildNumber" else baseVersion)
java.sourceCompatibility = JavaVersion.VERSION_11

docker {
    if (branchName != null) {
        url.set("unix:///var/run/docker.sock")

        registryCredentials {
            username.set(System.getenv("DOCKER_REGISTRY_USER"))
            password.set(System.getenv("DOCKER_REGISTRY_PASS"))
            url.set("https://$registryName/v2/")
        }
    }
}

sonarqube {
    if (branchName != null) {
        properties {
            property("sonar.projectKey", "aliquam20_luckperms-rest-api")
            property("sonar.organization", "aliquam")
            property("sonar.host.url", "https://sonarcloud.io")
            property("sonar.branch.name", branchName)
        }
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("io.ktor:ktor-server-netty:1.5.2")
    implementation("io.ktor:ktor-server-core:1.5.2")
    implementation("io.ktor:ktor-jackson:1.5.2")

    implementation("com.destroystokyo.paper:paper-api:1.16.5-R0.1-SNAPSHOT")
    implementation("net.luckperms:api:5.2")

    // microserver
    unzip("org.aliquam:alq-server-jumpstarter:0.5.2-SNAPSHOT-2")
    unzip("io.papermc:paper-server:267")
    unzip("net.milkbowl:vault:1.7.3")
    unzip("net.luckperms:luck-perms-bukkit:5.1.93")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

val buildArchiveDependencies = tasks.register<Copy>("buildArchiveDependencies") {
    dependsOn("jar")

    unzip.asFileTree.forEach {
        from(zipTree(it))
    }

    into(buildWorkDir)
}

val createDockerfile = tasks.register<Dockerfile>("createDockerfile") {
    from("adoptopenjdk:latest")
    copyFile("work/", "/server/")
    workingDir("/server/")
    runCommand("apt update && apt -y install python3")
    entryPoint("python3", "start.py")
    if (branchName == "develop" || branchName == null) {
        exposePort(2200, 2137)
    } else {
        exposePort(2200)
    }
}

val buildDockerImage = tasks.register<DockerBuildImage>("buildDockerImage") {
    dependsOn(createDockerfile)
    dependsOn(buildArchiveDependencies)

    doFirst {
        copy {
            from(buildWorkDir)
            into(buildDockerDir)
        }

        copy {
            from(serverConfigDir)
            into(buildDockerDir)
        }
    }

    when (branchName) {
        "master" -> {
            images.add("$registryName/$imageName:latest")
            images.add("$registryName/$imageName:$version")
        }
        "develop" -> {
            images.add("$registryName/$imageName:snapshot")
            images.add("$registryName/$imageName:$version")
        }
        else -> {
            images.add("$imageName:dev_build")
        }
    }
}

val pushDockerImage = tasks.register<DockerPushImage>("pushDockerImage") {
    when (branchName) {
        "master" -> {
            images.add("$registryName/$imageName:latest")
            images.add("$registryName/$imageName:$version")
        }
        "develop" -> {
            images.add("$registryName/$imageName:snapshot")
            images.add("$registryName/$imageName:$version")
        }
    }
}

tasks.withType<Jar> {
    val location = this.archiveFile
    enabled = true

    from("plugin.yml")
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })

    doLast {
        copy {
            from(location)
            into("$buildWorkDir/plugins")
        }
    }
}